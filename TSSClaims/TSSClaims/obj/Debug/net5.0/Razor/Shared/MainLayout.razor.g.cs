#pragma checksum "C:\Projects\TSSClaims\TSSClaims\Shared\MainLayout.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "df0e865218a347a72689f1a140082a5bc2998f55"
// <auto-generated/>
#pragma warning disable 1591
namespace TSSClaims.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using TSSClaims;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using TSSClaims.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using DevExpress.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using TSSClaims.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Projects\TSSClaims\TSSClaims\_Imports.razor"
using TSSClaims.Model;

#line default
#line hidden
#nullable disable
    public partial class MainLayout : LayoutComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<TSSClaims.Shared.Header>(0);
            __builder.AddAttribute(1, "StateCssClass", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 4 "C:\Projects\TSSClaims\TSSClaims\Shared\MainLayout.razor"
                              NavMenuCssClass

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(2, "StateCssClassChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => NavMenuCssClass = __value, NavMenuCssClass))));
            __builder.CloseComponent();
            __builder.AddMarkupContent(3, "\r\n\r\n");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "main");
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "sidebar" + " " + (
#nullable restore
#line 8 "C:\Projects\TSSClaims\TSSClaims\Shared\MainLayout.razor"
                         NavMenuCssClass

#line default
#line hidden
#nullable disable
            ));
            __builder.OpenComponent<TSSClaims.Shared.NavMenu>(8);
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.AddMarkupContent(9, "\r\n    ");
            __builder.OpenElement(10, "div");
            __builder.AddAttribute(11, "class", "content px-4");
            __builder.AddContent(12, 
#nullable restore
#line 12 "C:\Projects\TSSClaims\TSSClaims\Shared\MainLayout.razor"
         Body

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 15 "C:\Projects\TSSClaims\TSSClaims\Shared\MainLayout.razor"
      
    string NavMenuCssClass { get; set; }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
