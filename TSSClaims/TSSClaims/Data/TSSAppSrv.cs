﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TSSClaims.Model;


namespace TSSClaims.Data
{
    public class TSSAppSrv
    {

        TSSContext db = new TSSContext();

        
        public Task<List<LookupDto>> CompanyLookup()
        {
            return db.Entities
                .Where(x => x.IsActiveForClaims && x.CompanyEntityId == null)
                .Select(x => new LookupDto() { ID = x.EntityId, Name = x.Name })
                .ToListAsync();
        }

        //public Task<List<EntityDto>> ValueStreamLookup(int companyEntityID)
        //{
        //    return db.Entities
        //        .Where(x => x.IsActiveForClaims && x.CompanyEntityId == companyEntityID)
        //        .Select(x => new EntityDto() { EntityId = x.EntityId, Name = x.Name, StdKmTariff = x.StdKmTariff })
        //        .ToListAsync();
        //}
        public Task<List<Entity>> ValueStreamLookup(int companyEntityID)
        {
            return db.Entities
                .Where(x => x.IsActiveForClaims && x.CompanyEntityId == companyEntityID)
                //.Select(x => new EntityDto() { EntityId = x.EntityId, Name = x.Name, StdKmTariff = x.StdKmTariff })
                .ToListAsync();
        }
        public Task<List<Project>> ProjectForEntity(int entityID)
        {
            return db.Projects
                .Include(x => x.ProjectManagerEmployee)
                .Where(x => x.EntityId == entityID & (x.ProjectStatusId == 4 || x.ProjectStatusId == 7))
                .ToListAsync();
        }

        public Task<List<ClaimType>> ClaimTypeLookup(bool? isRecoverable)
        {
            return db.ClaimTypes
                .Where(x => x.IsRecoverable == isRecoverable)
                .ToListAsync();
        }

        public Task<List<LookupDto>> PastelAccountsLookup(int entityID, int claimTypeID)
        {
            return db.PastelAccounts
                .Where(x => x.IncludeInLookup == true && x.EntityId == entityID && x.PastelClaimTypes.Any(x => x.ClaimTypeId == claimTypeID))
                .Select(x => new LookupDto() { ID = x.PastelAccountId, Name = x.AccountDescription })
                .ToListAsync();
        }

        public Task<List<EmployeeDto>> EmployeeGetAll()
        {
            return db.Employees
                .Where(x => x.EmploymentStatusId == 1)
                .Select(x => new EmployeeDto() { ID = x.EmployeeId, Name = x.FullName, SalaryReference = x.SalaryReference })
                .ToListAsync();
        }
      

        public Task<List<ClaimHeader>> ClaimHeaderForEmployee(string domainName)
        {

           // domainName = "FOURIER\vroooyenj";  //#TB TODO Remove

            return db.ClaimHeaders
                .Include(x => x.Project)
                .Include(x => x.ClaimDetails)
                .Include(x=>x.Project.Entity)
                .Include(x=>x.ClaimantEmployee)
                .Include(x => x.Project.ProjectManagerEmployee)
                .Where(x => x.ClaimantEmployee.DomainName == domainName && x.ClaimProcessedDate == null)            
                .ToListAsync();
        }
       
        public void ClaimHeaderSave( ClaimHeader claimHeader)
        {
            // Company name prefix
            var comp = db.Entities.ToList();
            var company = comp.Where(x => x.Name.Equals(claimHeader.ClaimHeaderNumberCompany)).Select(x=>x.Abbreviation).FirstOrDefault();
            
            // End of company name prefix
           
            var detail = new List<ClaimDetail>();
           
            foreach (var item in claimHeader.ClaimDetails)
            {

                var newclaimdetail = new ClaimDetail();

                newclaimdetail.Amount = item.Amount;
                newclaimdetail.DateOfTransaction = DateTime.Now;
                newclaimdetail.ClaimDetailDescription = item.ClaimDetailDescription;
                newclaimdetail.ClaimTypeId = item.ClaimTypeId;
                
                detail.Add(newclaimdetail);
            

            }
         
            decimal total = detail.Sum(item => item.Amount);
           
              //get company name prefix and get claimheader number
          
            var claimnumber = db.ClaimHeaders.ToList();
            var number = claimnumber.Select(x => x.ClaimHeaderIdentity).LastOrDefault();
            number++;
         
            claimHeader.Amount = total;
            claimHeader.ClaimCaptureDate = DateTime.Now;
            claimHeader.ClaimHeaderNumber = company + number;
            claimHeader.ClaimHeaderNumberCompany = company;
            claimHeader.ClaimHeaderIdentity = number;


            if (db.ClaimHeaders.Any(o => o.ClaimHeaderId == claimHeader.ClaimHeaderId))         
           
            {               
                db.ClaimHeaders.Update(claimHeader);               
            }
            else
            {
                db.ClaimHeaders.Add(claimHeader);              
            }
            db.SaveChanges();

        }

        public void ClaimHeaderSubmit(ClaimHeader claimHeader)
        {
            claimHeader.ClaimSubmitDate = DateTime.Now;
            db.SaveChanges();
            
        }


    }

}
