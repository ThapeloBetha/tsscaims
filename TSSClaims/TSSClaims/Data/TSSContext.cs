﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TSSClaims.Model;

#nullable disable

namespace TSSClaims.Data
{
    public partial class TSSContext : DbContext
    {
        public TSSContext()
        {
        }

        public TSSContext(DbContextOptions<TSSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClaimCategory> ClaimCategories { get; set; }
        public virtual DbSet<ClaimDetail> ClaimDetails { get; set; }
        public virtual DbSet<ClaimDetailAttachment> ClaimDetailAttachments { get; set; }
        public virtual DbSet<ClaimHeader> ClaimHeaders { get; set; }
        public virtual DbSet<ClaimType> ClaimTypes { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Entity> Entities { get; set; }
        public virtual DbSet<PastelAccount> PastelAccounts { get; set; }
        public virtual DbSet<PastelClaimType> PastelClaimTypes { get; set; }
        public virtual DbSet<PastelClaimTypesMaster> PastelClaimTypesMasters { get; set; }
        public virtual DbSet<Project> Projects { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=Fapdev1;Initial Catalog=TSSV2_PROD_NEW;User ID=sa;Password=fourierfourier;Connect Timeout=450");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClaimCategory>(entity =>
            {
                entity.HasKey(e => e.ClaimCategory1);

                entity.ToTable("ClaimCategory");

                entity.Property(e => e.ClaimCategory1)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ClaimCategory");
            });

            modelBuilder.Entity<ClaimDetail>(entity =>
            {
                entity.ToTable("ClaimDetail");

                entity.Property(e => e.ClaimDetailId).HasColumnName("ClaimDetailID");

                entity.Property(e => e.Amount).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.ClaimDetailDescription)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimTypeId).HasColumnName("ClaimTypeID");

                entity.Property(e => e.DateOfTransaction).HasColumnType("datetime");

                entity.Property(e => e.HasAttachement).HasDefaultValueSql("((0))");

                entity.Property(e => e.PastelAccountId).HasColumnName("PastelAccountID");

                entity.Property(e => e.StdKmTariff).HasColumnType("decimal(38, 2)");

                entity.HasOne(d => d.ClaimHeaderNumberNavigation)
                    .WithMany(p => p.ClaimDetails)
                    .HasForeignKey(d => d.ClaimHeaderNumber)
                    .HasConstraintName("FK_ClaimDetail_ClaimHeader");

                entity.HasOne(d => d.ClaimType)
                    .WithMany(p => p.ClaimDetails)
                    .HasForeignKey(d => d.ClaimTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClaimDetail_ClaimType");

                entity.HasOne(d => d.PastelAccount)
                    .WithMany(p => p.ClaimDetails)
                    .HasForeignKey(d => d.PastelAccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClaimDetail_PastelAccount");
            });

            modelBuilder.Entity<ClaimDetailAttachment>(entity =>
            {
                entity.HasKey(e => e.ClaimDetailAttachementId);

                entity.ToTable("ClaimDetailAttachment");

                entity.Property(e => e.ClaimDetailAttachementId).HasColumnName("ClaimDetailAttachementID");

                entity.Property(e => e.BinaryData).HasColumnType("image");

                entity.Property(e => e.ClaimDetailId).HasColumnName("ClaimDetailID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.ClaimDetail)
                    .WithMany(p => p.ClaimDetailAttachments)
                    .HasForeignKey(d => d.ClaimDetailId)
                    .HasConstraintName("FK_ClaimDetailAttachment_ClaimDetail");
            });

            modelBuilder.Entity<ClaimHeader>(entity =>
            {
                entity.ToTable("ClaimHeader");

                entity.Property(e => e.ClaimHeaderId).HasColumnName("ClaimHeaderID");

                entity.Property(e => e.Amount).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.ApproverComment).IsUnicode(false);

                entity.Property(e => e.ApproverEmployeeId).HasColumnName("ApproverEmployeeID");

                entity.Property(e => e.ClaimHeaderNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasComputedColumnSql("([ClaimHeaderNumberCompany]+CONVERT([varchar](50),[ClaimHeaderIdentity],(0)))", false);

                entity.Property(e => e.ClaimHeaderNumberCompany)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimProcessedDate).HasColumnType("datetime");

                entity.Property(e => e.ClaimSubmitDate).HasColumnType("datetime");

                entity.Property(e => e.ClaimantEmployeeId).HasColumnName("ClaimantEmployeeID");

                entity.Property(e => e.ExportDate).HasColumnType("datetime");

                entity.Property(e => e.IsApproved).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.HasOne(d => d.ApproverEmployee)
                    .WithMany(p => p.ClaimHeaderApproverEmployees)
                    .HasForeignKey(d => d.ApproverEmployeeId)
                    .HasConstraintName("FK_ClaimHeader_Employees1");

                entity.HasOne(d => d.ClaimantEmployee)
                    .WithMany(p => p.ClaimHeaderClaimantEmployees)
                    .HasForeignKey(d => d.ClaimantEmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClaimHeader_Employees");

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.ClaimHeaders)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClaimHeader_Projects");
            });

            modelBuilder.Entity<ClaimType>(entity =>
            {
                entity.ToTable("ClaimType");

                entity.Property(e => e.ClaimTypeId).HasColumnName("ClaimTypeID");

                entity.Property(e => e.ClaimCategory)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClaimTypeDescription)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.ClaimCategoryNavigation)
                    .WithMany(p => p.ClaimTypes)
                    .HasForeignKey(d => d.ClaimCategory)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClaimType_ClaimCategory");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasIndex(e => new { e.FirstName, e.Surname }, "IX_Employees")
                    .IsUnique();

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.BirthDate).HasColumnType("datetime");

                entity.Property(e => e.BookingFrequencyIndicatory)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CellphoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DomainName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmploymentStatusId).HasColumnName("EmploymentStatusID");

                entity.Property(e => e.EmploymentTypeId).HasColumnName("EmploymentTypeID");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.EntityId).HasColumnName("EntityID");

                entity.Property(e => e.Extension)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ExternalRate).HasColumnType("money");

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .HasMaxLength(151)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(([firstname]+' ')+[surname])", false);

                entity.Property(e => e.HighExternalRate).HasColumnType("money");

                entity.Property(e => e.Idnumber)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("IDNumber");

                entity.Property(e => e.InternalRate).HasColumnType("money");

                entity.Property(e => e.IsClaimAdmin).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastIncrease).HasColumnType("datetime");

                entity.Property(e => e.LastPromotion).HasColumnType("datetime");

                entity.Property(e => e.LeaveDays).HasColumnType("numeric(18, 2)");

                entity.Property(e => e.LowExternalRate).HasColumnType("money");

                entity.Property(e => e.MentorEmployeeId).HasColumnName("MentorEmployeeID");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.OutstandingLeave)
                    .HasColumnType("decimal(18, 5)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PartnerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerTelephoneNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhysicalAddressLine1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PhysicalAddressLine2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PostId).HasColumnName("PostID");

                entity.Property(e => e.PostalAddressLine1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PostalAddressLine2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Province)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecoverableHours).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SalaryReference)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SalaryTariff).HasColumnType("money");

                entity.Property(e => e.SectionId).HasColumnName("SectionID");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.SupplierId).HasColumnName("SupplierID");

                entity.Property(e => e.Surname)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TelephoneNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Wdays)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("WDays");

                entity.Property(e => e.WorkHours).HasColumnType("decimal(10, 0)");

                entity.Property(e => e.YearlyLeave).HasColumnType("decimal(6, 2)");
            });

            modelBuilder.Entity<Entity>(entity =>
            {
                entity.HasIndex(e => e.Name, "IX_Entities")
                    .IsUnique();

                entity.Property(e => e.EntityId).HasColumnName("EntityID");

                entity.Property(e => e.Abbreviation)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ClientId)
                    .HasColumnName("ClientID")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CompanyEntityId).HasColumnName("CompanyEntityID");

                entity.Property(e => e.CostCodeSeries)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N/A')");

                entity.Property(e => e.Description)
                    .HasMaxLength(1024)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ParentEntityId).HasColumnName("ParentEntityID");

                entity.Property(e => e.ProjectGmtarget).HasColumnName("ProjectGMTarget");

                entity.Property(e => e.StdKmTariff).HasColumnType("decimal(38, 2)");

                entity.Property(e => e.Wacorwacr)
                    .HasColumnType("decimal(18, 2)")
                    .HasColumnName("WACORWACR");

                entity.Property(e => e.WeightedAverageCostRate).HasColumnType("decimal(18, 2)");
            });

            modelBuilder.Entity<PastelAccount>(entity =>
            {
                entity.ToTable("PastelAccount");

                entity.Property(e => e.PastelAccountId).HasColumnName("PastelAccountID");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AccountDescription)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CostCodeFilter)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EntityId).HasColumnName("EntityID");

                entity.Property(e => e.IncludeInLookup).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Entity)
                    .WithMany(p => p.PastelAccounts)
                    .HasForeignKey(d => d.EntityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PastelAccount_Entities");
            });

            modelBuilder.Entity<PastelClaimType>(entity =>
            {
                entity.ToTable("PastelClaimType");

                entity.Property(e => e.PastelClaimTypeId).HasColumnName("PastelClaimTypeID");

                entity.Property(e => e.ClaimTypeId).HasColumnName("ClaimTypeID");

                entity.Property(e => e.PastelAccountId).HasColumnName("PastelAccountID");

                entity.HasOne(d => d.ClaimType)
                    .WithMany(p => p.PastelClaimTypes)
                    .HasForeignKey(d => d.ClaimTypeId)
                    .HasConstraintName("FK_PastelClaimType_ClaimType");

                entity.HasOne(d => d.PastelAccount)
                    .WithMany(p => p.PastelClaimTypes)
                    .HasForeignKey(d => d.PastelAccountId)
                    .HasConstraintName("FK_PastelClaimType_PastelAccount1");
            });

            modelBuilder.Entity<PastelClaimTypesMaster>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("PastelClaimTypesMaster");

                entity.Property(e => e.ClaimTypeId).HasColumnName("ClaimTypeID");

                entity.Property(e => e.EntityId).HasColumnName("EntityID");

                entity.Property(e => e.PastelAccountId).HasColumnName("PastelAccountID");
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.HasIndex(e => e.ProjectCode, "IX_Projects")
                    .IsUnique();

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

                entity.Property(e => e.Approach)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N/A')");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ClientProjectManager)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ClientProjectSponsor)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ClientReferenceNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasMaxLength(4095)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.EntityId).HasColumnName("EntityID");

                entity.Property(e => e.InceptionReportUpdateDate).HasColumnType("datetime");

                entity.Property(e => e.LastChangeDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ParentProjectId).HasColumnName("ParentProjectID");

                entity.Property(e => e.PastelCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PortalUrl)
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.Property(e => e.ProblemStatement)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N/A')");

                entity.Property(e => e.ProgrammeId).HasColumnName("ProgrammeID");

                entity.Property(e => e.ProjectCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectManagerEmployeeId).HasColumnName("ProjectManagerEmployeeID");

                entity.Property(e => e.ProjectManagerEmployeeId2).HasColumnName("ProjectManagerEmployeeID2");

                entity.Property(e => e.ProjectStatusId).HasColumnName("ProjectStatusID");

                entity.Property(e => e.ProjectTypeId).HasColumnName("ProjectTypeID");

                entity.Property(e => e.Recoverable).HasDefaultValueSql("((0))");

                entity.Property(e => e.Scope)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N/A')");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.StrategicReviewUpdateDate).HasColumnType("datetime");

                entity.Property(e => e.TotalBillingEstimate).HasColumnType("money");

                entity.Property(e => e.TotalExpenseEstimate).HasColumnType("money");

                entity.Property(e => e.TotalInternalLabourCostEstimate).HasColumnType("money");

                entity.Property(e => e.WorkOrderNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Entity)
                    .WithMany(p => p.Projects)
                    .HasForeignKey(d => d.EntityId)
                    .HasConstraintName("FK_Projects_Entities");

                entity.HasOne(d => d.ProjectManagerEmployee)
                    .WithMany(p => p.ProjectProjectManagerEmployees)
                    .HasForeignKey(d => d.ProjectManagerEmployeeId)
                    .HasConstraintName("FK_Projects_Employees");

                entity.HasOne(d => d.ProjectManagerEmployeeId2Navigation)
                    .WithMany(p => p.ProjectProjectManagerEmployeeId2Navigations)
                    .HasForeignKey(d => d.ProjectManagerEmployeeId2)
                    .HasConstraintName("FK_Projects_Employees1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
