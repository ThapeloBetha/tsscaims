﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TSSClaims.Model;

namespace TSSClaims.Data
{
    [Route("api/[controller]")]
    public class UploadController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;


        public UploadController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        [Route("UploadFile/{claimDetailId}")]
        public ActionResult UploadFile(int claimDetailId, IFormFile myFile)
        {
           
            try
            {
                var path = Path.Combine(_hostingEnvironment.WebRootPath, "uploads/{claimDetailId}");
                if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
                using (var filestream = System.IO.File.Create(Path.Combine(path, myFile.FileName)))
                {
                    myFile.CopyTo(filestream);
                }
                

                using (TSSContext db = new TSSContext())
                {
                    var attachment = new ClaimDetailAttachment();
                  
                  
                        if (myFile.Length > 0)
                        {
                       
                        //get claimdetail Id
                        //var claimDetailList = db.ClaimDetails.ToList();
                        //var claimDetailId = claimDetailList.Select(x=>x.ClaimDetailId).LastOrDefault();
                        //end of claimdetail Id

                        using var filestream = myFile.OpenReadStream();
                        byte[] bytes = new byte[myFile.Length];
                        filestream.Read(bytes, 0, (int)myFile.Length);
                     
                        attachment.BinaryData = bytes;
                        attachment.FileName = myFile.FileName;
                        attachment.FileSize = Convert.ToInt32(myFile.Length);
                       // attachment.ClaimDetailId = claimDetailId;
                        }
                        
                        db.ClaimDetailAttachments.Add(attachment);
                    
                    db.SaveChanges();
                }
            }
            catch (Exception ex) 
            {
                
                
                var error = ex.Message;

                Response.StatusCode = 400;
            }
           
            return new EmptyResult();
        }
    }
}
