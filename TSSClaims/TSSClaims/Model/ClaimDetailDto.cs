﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace TSSClaims.Model
{
    class ClaimDetailDto
    {
        public ClaimDetailDto(ClaimDetail dataItem)
        {
            //DataItem = dataItem;
            //if (DataItem == null)
            //{
            //    DataItem = new ClaimDetail();
            //    IsNewRow = true;
            //}
            //Description = DataItem.Description;
            //Region = DataItem.Region;
            //City = DataItem.City;
        }

        //public ClaimDetail DataItem { get; set; }
        //public bool IsNewRow { get; set; }

        [Required]
        [StringLength(maximumLength: 32, MinimumLength = 4,
        ErrorMessage = "The description should be 4 to 32 characters.")]
        public string Description { get; set; }

        //string region;
        //[Required]
        //public string Region
        //{
        //    get => region;
        //    set
        //    {
        //        region = value;
        //        City = null;
        //        //OfficeLocations = VacancyRepository.GetOfficeLocationsByRegion(value).Select(x => x.City);
        //        StateHasChanged?.Invoke();
        //    }
        //}

        //[Required]
        //public string City { get; set; }

        //public IEnumerable<string> OfficeLocations = null;

        //public Action StateHasChanged { get; set; }
    }
}
