﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class EmployeeDto
    {
        public EmployeeDto()
        {
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string SalaryReference { get; set; }
    }
}
