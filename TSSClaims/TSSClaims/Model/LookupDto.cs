﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TSSClaims.Model
{
    public class LookupDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
