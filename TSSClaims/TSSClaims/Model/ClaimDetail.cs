﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace TSSClaims.Model
{
    public partial class ClaimDetail
    {
        public ClaimDetail()
        {
        }

        public int ClaimDetailId { get; set; }
        public int ClaimHeaderNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public DateTime DateOfTransaction { get; set; }
        [Required(ErrorMessage = "Required")]
        public string ClaimDetailDescription { get; set; }
        [Range(1, double.MaxValue, ErrorMessage = "Invalid")]
        public decimal Amount { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Invalid")] //#TB Test Validation
        public int PastelAccountId { get; set; } 
        [Range(1, double.MaxValue, ErrorMessage = "Invalid")]
        public double? TravelDistance { get; set; } = 0; //#TB Test Validation
        [Range(1, double.MaxValue, ErrorMessage = "Invalid")]
        public decimal? StdKmTariff { get; set; } = 0;
        public bool? IsVatTransaction { get; set; } = false;
        [Range(1, int.MaxValue, ErrorMessage = "Invalid")]
        public int ClaimTypeId { get; set; }
        public bool? HasAttachement { get; set; } = false;
        public virtual ClaimHeader ClaimHeaderNumberNavigation { get; set; }
        public virtual ClaimType ClaimType { get; set; }
        public virtual PastelAccount PastelAccount { get; set; }
        public virtual ICollection<ClaimDetailAttachment> ClaimDetailAttachments { get; set; } = new HashSet<ClaimDetailAttachment>();
    }
}
