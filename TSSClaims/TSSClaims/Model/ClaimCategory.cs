﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class ClaimCategory
    {
        public ClaimCategory()
        {
            ClaimTypes = new HashSet<ClaimType>();
        }

        public string ClaimCategory1 { get; set; }

        public virtual ICollection<ClaimType> ClaimTypes { get; set; }
    }
}
