﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class PastelAccount
    {
        public PastelAccount()
        {
            ClaimDetails = new HashSet<ClaimDetail>();
            PastelClaimTypes = new HashSet<PastelClaimType>();
        }

        public int PastelAccountId { get; set; }
        public string Account { get; set; }
        public string AccountDescription { get; set; }
        public int EntityId { get; set; }
        public bool? IncludeInLookup { get; set; }
        public string CostCodeFilter { get; set; }
        public virtual Entity Entity { get; set; }
        public virtual ICollection<ClaimDetail> ClaimDetails { get; set; }
        public virtual ICollection<PastelClaimType> PastelClaimTypes { get; set; }
    }
}
