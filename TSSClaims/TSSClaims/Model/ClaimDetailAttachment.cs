﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class ClaimDetailAttachment
    {
        public int ClaimDetailAttachementId { get; set; }
        public string FileName { get; set; }
        public int? FileSize { get; set; }
        public byte[] BinaryData { get; set; }
        public int ClaimDetailId { get; set; }

        public virtual ClaimDetail ClaimDetail { get; set; }
    }
}
