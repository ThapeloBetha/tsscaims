﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class ClaimType
    {
        public ClaimType()
        {
            ClaimDetails = new HashSet<ClaimDetail>();
            PastelClaimTypes = new HashSet<PastelClaimType>();
        }

        public int ClaimTypeId { get; set; }
        public string ClaimTypeDescription { get; set; }
        public bool IsVatTransaction { get; set; }
        public string ClaimCategory { get; set; }
        public bool IsRecoverable { get; set; }
        public virtual ICollection<ClaimDetail> ClaimDetails { get; set; }
        public virtual ClaimCategory ClaimCategoryNavigation { get; set; }
        public virtual ICollection<PastelClaimType> PastelClaimTypes { get; set; }
    }
}
