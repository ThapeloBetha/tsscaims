﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class PastelClaimType
    {
        public int PastelClaimTypeId { get; set; }
        public int PastelAccountId { get; set; }
        public int ClaimTypeId { get; set; }
        public virtual ClaimType ClaimType { get; set; }
        public virtual PastelAccount PastelAccount { get; set; }
    }
}
