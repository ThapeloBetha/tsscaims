﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class PastelClaimTypesMaster
    {
        public int PastelAccountId { get; set; }
        public int ClaimTypeId { get; set; }
        public int EntityId { get; set; }
        public bool? IncludeInLookup { get; set; }
    }
}
