﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class Employee
    {
        public Employee()
        {
            ClaimHeaderApproverEmployees = new HashSet<ClaimHeader>();
            ClaimHeaderClaimantEmployees = new HashSet<ClaimHeader>();
            ProjectProjectManagerEmployeeId2Navigations = new HashSet<Project>();
            ProjectProjectManagerEmployees = new HashSet<Project>();
        }

        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public int? SectionId { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
        public string Extension { get; set; }
        public string PhysicalAddressLine1 { get; set; }
        public string PhysicalAddressLine2 { get; set; }
        public string PostalAddressLine1 { get; set; }
        public string PostalAddressLine2 { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string TelephoneNumber { get; set; }
        public string CellphoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public decimal? InternalRate { get; set; }
        public decimal? ExternalRate { get; set; }
        public decimal? LeaveDays { get; set; }
        public string SalaryReference { get; set; }
        public decimal? OutstandingLeave { get; set; }
        public decimal? YearlyLeave { get; set; }
        public string BookingFrequencyIndicatory { get; set; }
        public decimal? WorkHours { get; set; }
        public decimal? RecoverableHours { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? EmploymentStatusId { get; set; }
        public int? EmploymentTypeId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? PostId { get; set; }
        public decimal? HighExternalRate { get; set; }
        public decimal? LowExternalRate { get; set; }
        public string Idnumber { get; set; }
        public string PartnerName { get; set; }
        public string PartnerTelephoneNumber { get; set; }
        public int? SupplierId { get; set; }
        public int? ClientId { get; set; }
        public int? EntityId { get; set; }
        public DateTime? LastIncrease { get; set; }
        public DateTime? LastPromotion { get; set; }
        public int? MentorEmployeeId { get; set; }
        public decimal? SalaryTariff { get; set; }
        public string FullName { get; set; }
        public string DomainName { get; set; }
        public bool? IsClaimAdmin { get; set; }
        public string Wdays { get; set; }
        public DateTime? ModifiedDateTime { get; set; }

        public virtual ICollection<ClaimHeader> ClaimHeaderApproverEmployees { get; set; }
        public virtual ICollection<ClaimHeader> ClaimHeaderClaimantEmployees { get; set; }
        public virtual ICollection<Project> ProjectProjectManagerEmployeeId2Navigations { get; set; }
        public virtual ICollection<Project> ProjectProjectManagerEmployees { get; set; }
    }
}
