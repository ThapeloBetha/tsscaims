﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class Project
    {
        public Project()
        {
            ClaimHeaders = new HashSet<ClaimHeader>();
        }

        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? EntityId { get; set; }
        public int? ProgrammeId { get; set; }
        public int? ProjectManagerEmployeeId { get; set; }
        public int? ProjectManagerEmployeeId2 { get; set; }
        public string ClientReferenceNumber { get; set; }
        public int? ClientId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ProjectStatusId { get; set; }
        public bool? LevelOfEffort { get; set; }
        public int? ParentProjectId { get; set; }
        public string PastelCode { get; set; }
        public string PortalUrl { get; set; }
        public string Comments { get; set; }
        public bool? Recoverable { get; set; }
        public bool? FixedCost { get; set; }
        public decimal? TotalExpenseEstimate { get; set; }
        public decimal? TotalBillingEstimate { get; set; }
        public decimal? TotalInternalLabourCostEstimate { get; set; }
        public string ProjectCode { get; set; }
        public int? CustomerSatisfactionValue { get; set; }
        public DateTime? InceptionReportUpdateDate { get; set; }
        public DateTime? LastChangeDate { get; set; }
        public int? PlanningFrequency { get; set; }
        public int? PlanningPeriod { get; set; }
        public int? PlanningTimeframe { get; set; }
        public DateTime? StrategicReviewUpdateDate { get; set; }
        public string WorkOrderNumber { get; set; }
        public string ClientProjectManager { get; set; }
        public string ClientProjectSponsor { get; set; }
        public int? ProjectTypeId { get; set; }
        public string ProblemStatement { get; set; }
        public string Scope { get; set; }
        public string Approach { get; set; }

        public virtual Entity Entity { get; set; }
        public virtual Employee ProjectManagerEmployee { get; set; }
        public virtual Employee ProjectManagerEmployeeId2Navigation { get; set; }
        public virtual ICollection<ClaimHeader> ClaimHeaders { get; set; }
    }
}
