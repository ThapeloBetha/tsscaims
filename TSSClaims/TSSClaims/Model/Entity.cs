﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class Entity
    {
        public Entity()
        {
            PastelAccounts = new HashSet<PastelAccount>();
            Projects = new HashSet<Project>();
        }

        public int EntityId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public int? ClientId { get; set; }
        public int? CustomerSatisfactionTarget { get; set; }
        public int? ProjectGmtarget { get; set; }
        public int? StrategicReviewInterval { get; set; }
        public int? SubContractingTarget { get; set; }
        public decimal? Wacorwacr { get; set; }
        public decimal? WeightedAverageCostRate { get; set; }
        public int? ParentEntityId { get; set; }
        public bool IsActiveForClaims { get; set; }
        public string CostCodeSeries { get; set; }
        public decimal? StdKmTariff { get; set; }
        public int? CompanyEntityId { get; set; }

        public virtual ICollection<PastelAccount> PastelAccounts { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
