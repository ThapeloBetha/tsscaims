﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TSSClaims.Model
{
    public partial class ClaimHeader
    {
        public ClaimHeader()
        {
        }

        public int ClaimHeaderId { get; set; }
        public int ClaimantEmployeeId { get; set; }
        public int ProjectId { get; set; }
        public decimal? Amount { get; set; }
        public int? ApproverEmployeeId { get; set; }
        public DateTime? ClaimCaptureDate { get; set; }
        public DateTime? ClaimProcessedDate { get; set; }
        public string ClaimHeaderNumber { get; set; }
        public bool? IsApproved { get; set; }
        public DateTime? ClaimSubmitDate { get; set; }
        public string ApproverComment { get; set; }
        public string ClaimHeaderNumberCompany { get; set; }
        public int? ClaimHeaderIdentity { get; set; }
        public DateTime? ExportDate { get; set; }

        public virtual Employee ApproverEmployee { get; set; }
        public virtual Employee ClaimantEmployee { get; set; }
        public virtual Project Project { get; set; }
        public virtual ICollection<ClaimDetail> ClaimDetails { get; set; } = new HashSet<ClaimDetail>();
    }
}
